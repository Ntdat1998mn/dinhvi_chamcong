const multer = require("multer");
/* const path = require("path"); */

const storage = multer.diskStorage({
  destination: function (req, file, res) {
    res(null, "./public/upload");
  },
  filename: function (req, file, res) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    /* const fileExtension = path.extname(file.originalname); */
    res(null, uniqueSuffix + "-" + file.fieldname + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

module.exports = {
  upload,
};
