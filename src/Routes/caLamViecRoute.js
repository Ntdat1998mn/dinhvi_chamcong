const express = require("express");
const { getCaLamViecList } = require("../controller/caLamViecController");

const caLamViecRoute = express.Router();

caLamViecRoute.get("/lay-danh-sach-ca-lam-viec", getCaLamViecList);

module.exports = caLamViecRoute;
