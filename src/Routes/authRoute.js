const express = require("express");
const { logIn } = require("../controller/authController");

const authRoute = express.Router();

authRoute.post("/dang-nhap", logIn);

module.exports = authRoute;
