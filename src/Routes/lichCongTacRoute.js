const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getLichCongTac,
  dangKyLichCongTac,
  huyDonXinCongTac,
} = require("../controller/lichCongTacController");

const lichCongTacRoute = express.Router();

lichCongTacRoute.get("/lay-lich-cong-tac", verifyToken, getLichCongTac);
lichCongTacRoute.post("/dang-ky-lich-cong-tac", verifyToken, dangKyLichCongTac);
lichCongTacRoute.put("/huy-lich-cong-tac/:id", verifyToken, huyDonXinCongTac);

module.exports = lichCongTacRoute;
