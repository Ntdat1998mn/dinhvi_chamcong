const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getLichTangCa,
  dangKyLichTangCa,
  getLichTangCaListByUserId,
  huyDonXinTangCa,
} = require("../controller/lichTangCaController");

const lichTangCaRoute = express.Router();

lichTangCaRoute.get("/lay-lich-tang-ca", verifyToken, getLichTangCa);
lichTangCaRoute.get(
  "/lay-lich-tang-ca-theo-nguoi-dung",
  verifyToken,
  getLichTangCaListByUserId
);
lichTangCaRoute.post("/dang-ky-lich-tang-ca", verifyToken, dangKyLichTangCa);
lichTangCaRoute.put("/huy-don-xin-tang-ca/:id", verifyToken, huyDonXinTangCa);

module.exports = lichTangCaRoute;
