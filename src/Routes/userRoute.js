const express = require("express");
const {
  getUserListByDepartmentId,
  updateUserInfor,
  layNguoiDungBanLanhDaoCongTy,
  uploadAvatar,
} = require("../controller/userController");
const { verifyToken } = require("../utils/verifyToken");
const { upload } = require("../utils/upload");

const userRoute = express.Router();

userRoute.post("/cap-nhat-thong-tin-nhan-vien", verifyToken, updateUserInfor);
userRoute.post(
  "/upload-avatar",
  verifyToken,
  upload.single("avatar"),
  uploadAvatar
);
userRoute.get(
  "/lay-danh-sach-nguoi-dung-theo-phong-ban/:id",
  verifyToken,
  getUserListByDepartmentId
);
userRoute.get(
  "/lay-danh-sach-nguoi-dung-ban-lanh-dao-cong-ty",
  verifyToken,
  layNguoiDungBanLanhDaoCongTy
);

module.exports = userRoute;
