const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getLichLamViecDangKy,
  dangKyLichLamViec,
} = require("../controller/lichLamViecDangKyController");

const lichLamViecDangKyRoute = express.Router();

lichLamViecDangKyRoute.get(
  "/lay-lich-lam-viec-dang-ky",
  verifyToken,
  getLichLamViecDangKy
);
lichLamViecDangKyRoute.post(
  "/dang-ky-lich-lam-viec",
  verifyToken,
  dangKyLichLamViec
);

module.exports = lichLamViecDangKyRoute;
