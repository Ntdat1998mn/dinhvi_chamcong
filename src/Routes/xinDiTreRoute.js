const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getXinDiTreList,
  xinDiTre,
  getXinDiTreListByUserId,
} = require("../controller/xinDiTreController");

const xinDiTreRoute = express.Router();

xinDiTreRoute.get("/lay-lich-xin-di-tre", verifyToken, getXinDiTreList);
xinDiTreRoute.get(
  "/lay-lich-xin-di-tre-theo-nhan-vien",
  verifyToken,
  getXinDiTreListByUserId
);
xinDiTreRoute.post("/xin-di-tre", verifyToken, xinDiTre);

module.exports = xinDiTreRoute;
