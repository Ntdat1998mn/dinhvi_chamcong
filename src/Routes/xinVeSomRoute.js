const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getXinVeSomList,
  xinVeSom,
  getXinVeSomListByUserId,
} = require("../controller/xinVeSomController");

const xinVeSomRoute = express.Router();

xinVeSomRoute.get("/lay-lich-xin-ve-som", verifyToken, getXinVeSomList);
xinVeSomRoute.get(
  "/lay-lich-xin-ve-som-theo-nhan-vien",
  verifyToken,
  getXinVeSomListByUserId
);
xinVeSomRoute.post("/xin-ve-som", verifyToken, xinVeSom);

module.exports = xinVeSomRoute;
