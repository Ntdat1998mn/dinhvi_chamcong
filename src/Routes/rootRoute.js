const express = require("express");
const authRoute = require("./authRoute");
const userRoute = require("./userRoute");
const chamCongRoute = require("./chamCongRoute");
const lichLamViecDangKyRoute = require("./lichLamViecDangKyRoute");
const lichLamViecDuocDuyetRoute = require("./lichLamViecDuocDuyetRoute");
const lichCongTacRoute = require("./lichCongTacRoute");
const lichTangCaRoute = require("./lichTangCaRoute");
const xinDiTreRoute = require("./xinDiTreRoute");
const caLamViecRoute = require("./caLamViecRoute");
const phepNghiRoute = require("./phepNghiRoute");
const xinVeSomRoute = require("./xinVeSomRoute");

const rootRoute = express.Router();

rootRoute.use("/auth", authRoute);
rootRoute.use("/nguoi-dung", userRoute);
rootRoute.use("/cham-cong", chamCongRoute);
rootRoute.use("/lich-lam-viec-dang-ky", lichLamViecDangKyRoute);
rootRoute.use("/lich-lam-viec-duoc-duyet", lichLamViecDuocDuyetRoute);
rootRoute.use("/lich-cong-tac", lichCongTacRoute);
rootRoute.use("/lich-tang-ca", lichTangCaRoute);
rootRoute.use("/xin-di-tre", xinDiTreRoute);
rootRoute.use("/ca-lam-viec", caLamViecRoute);
rootRoute.use("/phep-nghi", phepNghiRoute);
rootRoute.use("/xin-ve-som", xinVeSomRoute);

module.exports = rootRoute;
