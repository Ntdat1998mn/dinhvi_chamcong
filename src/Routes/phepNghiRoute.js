const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getPhepNghiList,
  getPhepNghiListByUserId,
  xinPhepNghi,
  huyDonNghiPhep,
  layNgayNghiTrongThang,
} = require("../controller/phepNghiController");

const phepNghiRoute = express.Router();

phepNghiRoute.get("/lay-lich-phep-nghi", verifyToken, getPhepNghiList);
phepNghiRoute.get(
  "/lay-ngay-nghi-trong-thang",
  verifyToken,
  layNgayNghiTrongThang
);
phepNghiRoute.get(
  "/lay-lich-phep-nghi-theo-nhan-vien",
  verifyToken,
  getPhepNghiListByUserId
);
phepNghiRoute.post("/xin-phep-nghi", verifyToken, xinPhepNghi);
phepNghiRoute.put("/huy-don-xin-phep/:id", verifyToken, huyDonNghiPhep);

module.exports = phepNghiRoute;
