const express = require("express");
const {
  inShift,
  outShift,
  getShiftByMonth,
} = require("../controller/chamCongController");
const { verifyToken } = require("../utils/verifyToken");

const chamCongRoute = express.Router();

chamCongRoute.post("/lay-cham-cong-theo-thang", verifyToken, getShiftByMonth);
chamCongRoute.post("/vao-ca", verifyToken, inShift);
chamCongRoute.post("/ra-ca", verifyToken, outShift);

module.exports = chamCongRoute;
