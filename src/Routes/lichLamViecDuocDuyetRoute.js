const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  getLichLamViecDuocDuyet,
  dangKyLichLamViec,
} = require("../controller/lichLamViecDuocDuyetController");

const lichLamViecDuocDuyetRoute = express.Router();

lichLamViecDuocDuyetRoute.get(
  "/lay-lich-lam-viec-duoc-duyet",
  verifyToken,
  getLichLamViecDuocDuyet
);
/* lichLamViecDuocDuyetRoute.post(
  "/duyet-lich-lam-viec",
  verifyToken,
  dangKyLichLamViec
); */

module.exports = lichLamViecDuocDuyetRoute;
