const { PrismaClient } = require("@prisma/client");

const { successCode, errorCode, notFoundCode } = require("../config/response");

const prisma = new PrismaClient();

// Lấy danh sách ca làm việc
const getCaLamViecList = async (req, res) => {
  try {
    let data = await prisma.dvcc_ca_lam_viec.findMany();
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(res, data, "Lấy ca làm việc thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = { getCaLamViecList };
