const { PrismaClient } = require("@prisma/client");

const { successCode, errorCode, notFoundCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy lịch làm việc được xếp
const getLichLamViecDuocDuyet = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;

    let data = await prisma.dvcc_lich_lam_viec_duoc_duyet.findMany({
      where: {
        nv_id,
      },
      orderBy: {
        ngay: "desc",
      },
      include: {
        dvcc_ca_lam_viec: true,
      },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(res, data, "Lấy lịch được duyệt thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// được duyệt lịch
/* const dangKyLichLamViec = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const inputData = req.body;
    if (inputData.length === 0) {
      return failCode(res, "Bạn có chắc không làm ngày nào trong tuần!");
    }
    let data = [];
    let newData;
    for (const item of inputData) {
      item.ca_lam_viec_id *= 1;
      // Tìm kiếm xem có lịch nào trùng lịch đang đăn ký không
      let lichDangKy = await prisma.lich_lam_viec_duoc_duyet.findFirst({
        where: {
          nv_id,
          ngay: item.ngay,
        },
      });
      // Nếu có thì update, không thì sẽ tạo
      if (lichDangKy) {
        newData = await prisma.lich_lam_viec_duoc_duyet.update({
          data: {
            ngay: item.ngay,
            ca_lam_viec_id: item.ca_lam_viec_id,
          },
          where: {
            id: lichDangKy.id,
          },
        });
        newData = { ...newData, status: "Đã cập nhật lịch thành công!" };
      } else {
        newData = await prisma.lich_lam_viec_duoc_duyet.create({
          data: {
            nv_id,
            ngay: item.ngay,
            ca_lam_viec_id: item.ca_lam_viec_id,
          },
        });
        newData = { ...newData, status: "Đã được duyệt lịch thành công!" };
      }
      data.push(newData);
    }
    prisma.$disconnect;
    if (data.length === 0) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(res, data, "được duyệt và cập nhật lịch thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
}; */

module.exports = { getLichLamViecDuocDuyet /* dangKyLichLamViec */ };
