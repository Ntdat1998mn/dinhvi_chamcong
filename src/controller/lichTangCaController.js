const { PrismaClient } = require("@prisma/client");

const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy lịch tăng ca
const getLichTangCa = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;

    let data = await prisma.dvcc_lich_tang_ca.findMany({
      where: {
        nv_id,
        status: { not: 0 },
      },
      orderBy: {
        ngay_bat_dau: "asc",
      },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(res, data, "Lấy lịch tăng ca thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Lấy lịch tăng ca theo người dùng
const getLichTangCaListByUserId = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    let data = await prisma.dvcc_lich_tang_ca.findMany({
      where: { nv_id, status: { not: 0 } },
      include: {
        thong_tin_nguoi_duyet: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
        thong_tin_nguoi_quan_ly_truc_tiep: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
      },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(
        res,
        data,
        "Lấy danh sách tăng ca theo nhân viên thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Đăng ký lịch
const dangKyLichTangCa = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const inputData = req.body;
    inputData.nguoi_duyet *= 1;
    inputData.quan_ly_truc_tiep *= 1;
    data = await prisma.dvcc_lich_tang_ca.create({
      data: {
        nv_id,
        ...inputData,
      },
    });
    prisma.$disconnect;
    if (!data) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(res, data, "Đăng ký lịch tăng ca thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Huỷ đơn xin tăng ca
const huyDonXinTangCa = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const id = req.params.id * 1;
    let tangCa = await prisma.dvcc_lich_tang_ca.findFirst({
      where: { nv_id, id },
    });

    if (!tangCa) {
      return failCode(res, "Đơn xin phép tăng ca đã được huỷ!");
    } else {
      const today = new CustomDate();
      const ngayBatDau = new Date(tangCa.ngay_bat_dau);
      if (today > ngayBatDau) {
        return failCode(res, "Bạn không thể huỷ đơn xin tăng ca đã quá hạn!");
      }
    }
    let data = await prisma.dvcc_lich_tang_ca.update({
      where: { nv_id, id },
      data: { status: 0 },
    });
    prisma.$disconnect;
    if (!data) {
      return notFoundCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(
        res,
        data,
        "Huỷ đơn xin tăng ca thành công thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Hàm kiểm tra và cập nhật trạng thái đơn xin tăng ca
const updateStatusOfTangCa = async () => {
  try {
    const today = new CustomDate();
    let formatToday = formatDateYYYYMMDD(today);
    const tangCaList = await prisma.dvcc_lich_tang_ca.findMany({
      where: {
        OR: [{ status: 1 }, { status: 2 }],
        ngay_bat_dau: {
          lte: formatToday,
        },
      },
    });
    if (tangCaList.length === 0) {
      prisma.$disconnect;
    } else {
      for (const tangCa of tangCaList) {
        await prisma.dvcc_lich_tang_ca.update({
          where: { id: tangCa.id },
          data: { status: 4 },
        });
      }
      prisma.$disconnect;
    }
  } catch (err) {}
};

module.exports = {
  getLichTangCa,
  dangKyLichTangCa,
  getLichTangCaListByUserId,
  huyDonXinTangCa,
  updateStatusOfTangCa,
};
