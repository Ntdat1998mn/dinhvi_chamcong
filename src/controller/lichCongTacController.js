const { PrismaClient } = require("@prisma/client");

const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy lịch công tác
const getLichCongTac = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;

    let data = await prisma.dvcc_lich_cong_tac.findMany({
      where: {
        nv_id,
        status: { not: 0 },
      },
      orderBy: {
        ngay_bat_dau: "asc",
      },
      include: {
        thong_tin_nguoi_duyet: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
        thong_tin_nguoi_quan_ly_truc_tiep: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
      },
    });
    prisma.$disconnect;
    if (data.length !== 0) {
      return successCode(res, data, "Lấy lịch công tác thành công!");
    } else {
      return failCode(res, "Không có lịch công tác nào!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Đăng ký lịch
const dangKyLichCongTac = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const inputData = req.body;
    inputData.nguoi_duyet *= 1;
    inputData.quan_ly_truc_tiep *= 1;
    data = await prisma.dvcc_lich_cong_tac.create({
      data: {
        nv_id,
        ...inputData,
      },
    });
    prisma.$disconnect;
    if (!data) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(res, data, "Đăng ký lịch công tác thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Huỷ đơn xin công tác
const huyDonXinCongTac = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const id = req.params.id * 1;
    let congTac = await prisma.dvcc_lich_cong_tac.findFirst({
      where: { nv_id, id, status: { not: 0 } },
    });

    if (!congTac) {
      return failCode(res, "Đơn xin phép đi công tác không tồn tại!");
    } else {
      const today = new CustomDate();
      const ngayBatDau = new Date(congTac.ngay_bat_dau);
      if (today > ngayBatDau) {
        return failCode(
          res,
          "Bạn không thể huỷ đơn xin phép đi công tác đã quá hạn!"
        );
      }
    }
    let data = await prisma.dvcc_lich_cong_tac.update({
      where: { nv_id, id },
      data: { status: 0 },
    });
    prisma.$disconnect;
    if (!data) {
      return notFoundCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(
        res,
        data,
        "Huỷ đơn xin phép đi công tác thành công thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Hàm kiểm tra và cập nhật trạng thái đơn xin công tác
const updateStatusOfCongTac = async () => {
  try {
    const today = new CustomDate();
    let formatToday = formatDateYYYYMMDD(today);
    const congTacList = await prisma.dvcc_lich_cong_tac.findMany({
      where: {
        OR: [{ status: 1 }, { status: 2 }],
        ngay_bat_dau: {
          lte: formatToday,
        },
      },
    });
    if (congTacList.length === 0) {
      prisma.$disconnect;
    } else {
      for (const congTac of congTacList) {
        await prisma.dvcc_lich_cong_tac.update({
          where: { id: congTac.id },
          data: { status: 4 },
        });
      }
      prisma.$disconnect;
    }
  } catch (err) {}
};
setInterval(() => {
  updateStatusOfCongTac();
}, 1000);

module.exports = {
  getLichCongTac,
  dangKyLichCongTac,
  huyDonXinCongTac,
  updateStatusOfCongTac,
};
