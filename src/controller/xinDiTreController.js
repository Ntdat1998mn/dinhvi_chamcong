const { PrismaClient } = require("@prisma/client");
const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
} = require("../config/response");

const prisma = new PrismaClient();

// Lấy toàn bộ lịch xin đi trễ
const getXinDiTreList = async (req, res) => {
  try {
    let data = await prisma.dvcc_xin_di_tre.findMany();
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(
        res,
        data,
        "Lấy danh sách lịch xin đi trễ thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Lấy lịch xin đi trể theo nv_id
const getXinDiTreListByUserId = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    let data = await prisma.dvcc_xin_di_tre.findMany({
      where: { nv_id },
      include: {
        thong_tin_nguoi_duyet: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
        thong_tin_nguoi_quan_ly_truc_tiep: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
      },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(
        res,
        data,
        "Lấy danh sách lịch xin đi trễ theo nhân viên thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Xin đi trễ
const xinDiTre = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const inputData = req.body;
    let data = await prisma.dvcc_xin_di_tre.create({
      data: {
        ...inputData,
        nv_id,
      },
    });

    prisma.$disconnect;
    if (!data) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(
        res,
        data,
        "Xin đi trễ thành công, đợi cấp trên duyệt!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = { getXinDiTreList, xinDiTre, getXinDiTreListByUserId };
