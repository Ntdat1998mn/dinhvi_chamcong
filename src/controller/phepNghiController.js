const { PrismaClient } = require("@prisma/client");
const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy toàn bộ phép nghỉ
const getPhepNghiList = async (req, res) => {
  try {
    let data = await prisma.dvcc_phep_nghi.findMany({
      where: { phepnghi_status: { not: 0 } },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(res, data, "Lấy danh sách phép nghỉ thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Lấy phép nghỉ theo nv_id
const getPhepNghiListByUserId = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    let data = await prisma.dvcc_phep_nghi.findMany({
      where: { nv_id, phepnghi_status: { not: 0 } },
      include: {
        thong_tin_nguoi_duyet: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
        thong_tin_nguoi_quan_ly_truc_tiep: {
          select: {
            nv_id: true,
            nv_name: true,
          },
        },
      },
    });
    prisma.$disconnect;
    if (data.length === 0) {
      return notFoundCode(res, "Không có dữ liệu!");
    } else {
      return successCode(
        res,
        data,
        "Lấy danh sách phép nghỉ theo nhân viên thành công!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Huỷ đơn xin nghỉ phép
const huyDonNghiPhep = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const id = req.params.id * 1;
    let phepNghi = await prisma.dvcc_phep_nghi.findFirst({
      where: { nv_id, id, phepnghi_status: { not: 0 } },
    });

    if (!phepNghi) {
      return failCode(res, "Đơn xin phép đã được huỷ!");
    } else {
      const today = new CustomDate();
      const ngayBatDau = new Date(phepNghi.ngay_bat_dau);
      if (today > ngayBatDau) {
        return failCode(res, "Bạn không thể huỷ đơn xin phép đã quá hạn!");
      }
    }
    let data = await prisma.dvcc_phep_nghi.update({
      where: { nv_id, id },
      data: { phepnghi_status: 0 },
    });
    prisma.$disconnect;
    if (!data) {
      return notFoundCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(res, data, "Huỷ đơn xin phép thành công thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Xin phép nghỉ
const xinPhepNghi = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    const inputData = req.body;
    inputData.quan_ly_truc_tiep *= 1;
    inputData.nguoi_duyet *= 1;
    inputData.ca_bat_dau *= 1;
    inputData.ca_ket_thuc *= 1;
    let data = await prisma.dvcc_phep_nghi.create({
      data: {
        ...inputData,
        nv_id,
      },
    });

    prisma.$disconnect;
    if (!data) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else {
      return successCode(
        res,
        data,
        "Xin phép nghỉ thành công, đợi cấp trên duyệt!"
      );
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Hàm kiểm tra và cập nhật trạng thái đơn xin nghỉ phép
const updateStatusOfPhepNghi = async () => {
  try {
    const today = new CustomDate();
    let formatToday = formatDateYYYYMMDD(today);
    const phepNghiList = await prisma.dvcc_phep_nghi.findMany({
      where: {
        OR: [{ phepnghi_status: 1 }, { phepnghi_status: 2 }],
        ngay_bat_dau: {
          lte: formatToday,
        },
      },
    });
    if (phepNghiList.length === 0) {
      prisma.$disconnect;
    } else {
      for (const phepNghi of phepNghiList) {
        await prisma.dvcc_phep_nghi.update({
          where: { id: phepNghi.id },
          data: { phepnghi_status: 4 },
        });
      }
      prisma.$disconnect;
    }
  } catch (err) {}
};

// Lấy ngày nghỉ tính lương
const layNgayNghiTrongThang = async (req, res) => {
  try {
    return successCode(
      res,
      { tong_ngay_nghi_co_luong: 4, tong_ngay_nghi_khong_luong: 4 },
      "Lấy ngày nghỉ thành công!"
    );
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = {
  getPhepNghiList,
  getPhepNghiListByUserId,
  xinPhepNghi,
  huyDonNghiPhep,
  updateStatusOfPhepNghi,
  layNgayNghiTrongThang,
};
