const { PrismaClient } = require("@prisma/client");
const { createToken } = require("../utils/verifyToken");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Đăng nhập
const logIn = async (req, res) => {
  try {
    let { nv_password, nv_username } = req.body;
    let data = await prisma.ns_nhanvien.findFirst({
      where: {
        nv_username,
      },

      select: {
        nv_id: true,
        nv_name: true,
        danhmuc_id: true,
        nv_chucvu: true,
        nv_ngaysinh: true,
        nv_ngayvaolam: true,
        nv_sdt_lienhe: true,
        nv_email: true,
        nv_avatar: true,
        nv_calamviec: true,
        nv_diachithuongtru: true,
        nv_diachitamtru: true,
        nv_username: true,
        nv_password: true,
        ns_danhmuc_phongban: {
          select: {
            danhmuc_id: true,
            danhmuc_name: true,
            thong_tin_company: {
              select: {
                company_id: true,
                company_name: true,
              },
            },
          },
        },
        dvcc_chi_nhanh: {
          select: {
            chi_nhanh_id: true,
            chi_nhanh_name: true,
            chi_nhanh_address: true,
            latitude: true,
            longitude: true,
          },
        },
        dvcc_chuc_vu: {
          select: {
            chuc_vu_id: true,
            chuc_vu_name: true,
          },
        },
      },
    });
    prisma.$disconnect();
    if (data) {
      if (nv_password == data.nv_password) {
        let token = createToken(data);
        const { nv_password, ...content } = data;
        return successCode(res, { ...content, token }, "Đăng nhập thành công!");
      } else {
        nv_password = "";
        return failCode(res, "Mật khẩu không đúng!");
      }
    } else {
      return failCode(res, "Tài khoản không tồn tại!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = { logIn };
