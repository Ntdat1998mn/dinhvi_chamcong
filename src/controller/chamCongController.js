const { PrismaClient } = require("@prisma/client");

const { successCode, errorCode, failCode } = require("../config/response");
const {
  formatTimeHHMMSS,
  formatDateYYYYMMDD,
  CustomDate,
} = require("../utils/handleDate");
const prisma = new PrismaClient();

// Lấy toàn bộ chấm công theo tháng
const getShiftByMonth = async (req, res) => {
  try {
    /* const nv_id = req.user.content.nv_id; */
    let inputData = req.body;
    let data = await prisma.dvcc_cham_cong.findMany({
      where: {
        ngay: {
          gte: `${inputData.year}-${inputData.month
            .toString()
            .padStart(2, "0")}-01`,
          lt: `${inputData.year}-${(inputData.month + 1)
            .toString()
            .padStart(2, "0")}-01`,
        },
      },
    });
    return successCode(res, data, "Lấy chấm công theo tháng thành công");
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Vào ca
const inShift = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    let currentDate = new CustomDate();
    let ngay = formatDateYYYYMMDD(currentDate);

    let dataInShift = await prisma.dvcc_cham_cong.findFirst({
      where: { nv_id, ngay },
    });
    if (dataInShift) {
      prisma.$disconnect();
      return failCode(res, "Bạn đã chấm vào ca trước đó");
    }
    let gio_den = formatTimeHHMMSS(currentDate);

    let data = {
      nv_id,
      ngay,
      gio_den,
    };

    let newTimeKeeping = await prisma.dvcc_cham_cong.create({ data });
    prisma.$disconnect();
    return successCode(res, newTimeKeeping, "Vào ca thành công!");
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Ra ca
const outShift = async (req, res) => {
  try {
    const nv_id = req.user.content.nv_id;
    let currentDate = new CustomDate();
    let ngay = formatDateYYYYMMDD(currentDate);

    let dataOutShift = await prisma.dvcc_cham_cong.findFirst({
      where: { nv_id, ngay },
    });
    if (!dataOutShift) {
      prisma.$disconnect();
      return failCode(res, "Bạn không vào ca trước đó");
    }
    let gio_di = formatTimeHHMMSS(currentDate);

    let newTimeKeeping = await prisma.dvcc_cham_cong.update({
      data: {
        gio_di: gio_di,
      },
      where: {
        cham_cong_id: dataOutShift.cham_cong_id,
      },
    });
    prisma.$disconnect();
    return successCode(res, newTimeKeeping, "Ra ca thành công!");
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = { inShift, outShift, getShiftByMonth };
