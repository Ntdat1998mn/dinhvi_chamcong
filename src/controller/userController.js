const { PrismaClient } = require("@prisma/client");
const { createToken } = require("../utils/verifyToken");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Cập nhật thông tin người dùng
const updateUserInfor = async (req, res) => {
  try {
    const user = req.user.content;
    const inputData = req.body;

    if (inputData.nv_username) {
      return failCode(res, "Không thể thay đổi tên tài khoản!");
    }
    if (inputData.oldPassword | inputData.newPassword) {
      const existingUser = await prisma.ns_nhanvien.findUnique({
        where: {
          nv_id: user.nv_id,
        },
      });

      if (existingUser && existingUser.nv_password !== inputData.oldPassword) {
        return failCode(res, "Mật khẩu cũ không đúng!");
      }

      // Kiểm tra mật khẩu mới có đủ độ dài
      if (inputData.newPassword.trim().length < 8) {
        return failCode(res, "Mật khẩu mới phải có ít nhất 8 ký tự!");
      }
      await prisma.ns_nhanvien.update({
        where: {
          nv_id: user.nv_id,
        },
        data: { nv_password: inputData.newPassword },
      });
      prisma.$disconnect();
      successCode(res, "", "Cập nhật mật khẩu thành công!");
    }

    if (inputData.nv_username) {
      return failCode(res, "Không thể thay đổi tên tài khoản!");
    }
    if (inputData.nv_password && inputData.nv_password.trim().length < 8) {
      return failCode(res, "Mật khẩu phải có ít nhất 8 chữ số");
    }
    let newData = await prisma.ns_nhanvien.update({
      where: {
        nv_id: user.nv_id,
      },
      data: inputData,
    });
    prisma.$disconnect();
    return successCode(
      res,
      newData,
      "Cập nhật thông tin nhân viên thành công!"
    );
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// lấy danh sách người dùng theo id phòng ban
const getUserListByDepartmentId = async (req, res) => {
  try {
    let { id } = req.params;

    let data = await prisma.ns_nhanvien.findMany({
      where: { danhmuc_id: id * 1 },
    });
    prisma.$disconnect();
    if (data.length > 0) {
      return successCode(
        res,
        data,
        "Lấy danh sách người dùng cùng phòng thành công!"
      );
    } else {
      return failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// lấy danh sách người dùng ban lãnh đạo theo công ty
const layNguoiDungBanLanhDaoCongTy = async (req, res) => {
  try {
    const company_id =
      req.user.content.ns_danhmuc_phongban.thong_tin_company.company_id;

    let thongTinPhongLanhDao = await prisma.ns_danhmuc_phongban.findFirst({
      where: { company: company_id, danhmuc_name: "Ban Lãnh Đạo" },
    });
    let data = null;
    if (thongTinPhongLanhDao) {
      data = await prisma.ns_nhanvien.findMany({
        where: { danhmuc_id: thongTinPhongLanhDao.danhmuc_id },
      });
    }
    prisma.$disconnect();
    if (data.length > 0) {
      return successCode(
        res,
        data,
        "Lấy danh sách người dùng cùng phòng thành công!"
      );
    } else {
      return failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// upload avatar //
const uploadAvatar = async (req, res) => {
  try {
    let avatar = req.file;
    if (!avatar) {
      return failCode(res, "Không có ảnh nào được tải lên!");
    }
    const user = req.user.content;

    const dataUser = await prisma.ns_nhanvien.findFirst({
      where: { nv_id: user.nv_id },
    });
    if (!dataUser) {
      prisma.$disconnect();
      return notFoundCode(res, "Người dùng không tồn tại!");
    }
    const baseUrl = `${req.protocol}://${req.get("host")}`;
    const inputData = {
      nv_avatar: `${avatar.destination}/${avatar.filename}`,
    };
    let newData = await prisma.ns_nhanvien.update({
      where: {
        nv_id: user.nv_id,
      },
      data: inputData,
    });
    prisma.$disconnect();
    inputData.nv_avatar = baseUrl + "/" + inputData.nv_avatar;
    return successCode(
      res,
      { nv_avatar: newData.nv_avatar },
      "Thêm tệp thành công!"
    );
  } catch (err) {
    return errorCode(res, err.message);
  }
};

module.exports = {
  getUserListByDepartmentId,
  updateUserInfor,
  layNguoiDungBanLanhDaoCongTy,
  uploadAvatar,
};
